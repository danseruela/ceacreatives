(function($) {

$(document).ready(function(){
	
	// ===== Same page anchor link effects
	$('.menu-item a, #getquote').click(function(e) {
		e.preventDefault();
		var aid = $(this).attr("href");
		
		// Check if link is of the same page
    	if (this.pathname == window.location.pathname &&
		    this.protocol == window.location.protocol &&
			this.host == window.location.host && 
			aid.includes("#")) {
			$('html,body').animate({
	    		scrollTop: $(aid).offset().top - 90
			}, 100);
			// Test
			console.log("Pathname: " + this.pathname + " = " + window.location.pathname);
			console.log("Protocol: " + this.protocol + " = " + window.location.protocol);
			console.log("Host: " + this.host + " = " + window.location.host);
			console.log(aid);
		} else {
			console.log(aid);
			window.location.href = aid;
		}
	});

	// Add attrib for navbar main menu links in mobile view
	function mediaSize() {
		if(window.matchMedia('(max-width: 425px)').matches) {
			$('#navbar-main #menu-main-menu li a').attr({
				'data-toggle' : 'collapse',
				'data-target' : '#navbarSupportedContent'
			});
		} else {
			$('#navbar-main #menu-main-menu li a').removeAttr('data-toggle data-target');
		}
	}
	// call function
	mediaSize();

	// Attach the function to the resize event listener
	window.addEventListener('resize', mediaSize, false);

});

})(jQuery);
	