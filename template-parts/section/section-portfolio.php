<?php
/**
 * The template for displaying the Portfolio Section
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */
?>
<section id="awards" class="card-deck awards text-center">
    <h1>AWARDS / RECOGNITIONS</h1><br>
    <!-- <p class="text-muted"><em>Organizations and Brands we've worked with:</em></p> -->
</section>
<div class="card-deck text-center">
    <img class="img-fluid" style="width: 500px; height: auto; margin: 2rem auto 0;" src="<?php echo get_template_directory_uri(); ?>/img/awards/Mevents2021_banners bronze.png" alt="The Marketing Awards 2021 Singapore">
    <img class="img-fluid portfolio-member" style="width: auto;" src="<?php echo get_template_directory_uri(); ?>/img/awards/bestofcebu2020.png" alt="Best of Cebu 2020">
</div>
<div class="spacer"></div>
<div class="spacer"></div>
<section id="portfolio" class="card-deck portfolio text-center">
    <h1>PORTFOLIO</h1><br>
    <p class="text-muted"><em>Organizations and Brands we've worked with:</em></p>
</section>
<div class="card-deck portfolio-slide">
    <div id="carouselPortfolioControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item text-center active">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/abscbn.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/asean50.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/ayala-center-cebu.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/ayala-central-bloc.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/cafe-dessart.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/cast-2017-cebu-philippine.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/cebu-province-logo.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/chocolate-chamber.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
        </div>
        <div class="carousel-item text-center">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/Converge_ExpBetter.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/electric-entertainment.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/foodpanda.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/globe.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/mactan-alfresco.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/mactan-newtown.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/Monark.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/parkmall.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
        </div>
        <div class="carousel-item text-center">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/philippine-ocean-conference.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/smart.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/SM-Cebu.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/SM-Seaside.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/the-big-difference.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/uber.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/zoomcar.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/grab-ph.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
        </div>
        <div class="carousel-item text-center">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/hiyas-ng-pilipinas.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/WIDIA.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/APG.png" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
            <img src="<?php echo get_template_directory_uri(); ?>/img/logos/uv-gcm.jpg" class="d-inline-block w-9 ml-3 mr-3 mt-5 mb-5" alt="...">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselPortfolioControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselPortfolioControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    </div>
</div>
<div id="accreditation" class="card-deck text-center">
    <h6 style="width: inherit;">MEMBER & ACCREDITED BY:</h6>
    <img class="portfolio-member" src="<?php echo get_template_directory_uri(); ?>/img/accreditations/leap.jpg" alt="LEAP">
    <img class="portfolio-member" src="<?php echo get_template_directory_uri(); ?>/img/accreditations/dun-bradstreet.jpg" alt="Dun and Bradstreet">
    <img class="portfolio-member" src="<?php echo get_template_directory_uri(); ?>/img/accreditations/PhilGEPS-logo.png" alt="PhilGEPS">
</div>
<div class="spacer"></div>

