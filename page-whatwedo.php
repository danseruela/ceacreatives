<?php
/**
 * Template Name: What We Do Page Template
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */

get_header(); 

    $marketing = new WP_Query( array(
        'category_name' => 'marketing',
        'posts_per_page' => 100,
    ) );

    $events = new WP_Query( array(
        'category_name' => 'events',
        'posts_per_page' => 100,
    ) );

    $creative = new WP_Query( array(
        'category_name' => 'creative',
        'posts_per_page' => 100,
    ) );

    $talent = new WP_Query( array(
        'category_name' => 'talent',
        'posts_per_page' => 100,
    ) );
?>
<div id="whatwedo-page" class="card-deck">
    <h1 class="text-center">What We Do</h1><br>
    <p class="text-center text-muted" style="margin: 0 auto;">
        We are passionate with what we do, and we make sure that we deliver the requirements that our clients need. <br>
        We offer a wide array of well packaged Creative and Marketing services.
    </p>
    <!-- Tab Menus for the CEA Creatives Services -->
    <nav class="nav nav-pills nav-justified card-deck" role="tablist">
        <a class="nav-item nav-link active" id="pills-marketing-tab" href="#pills-marketing" data-toggle="pill" role="tab" aria-controls="pills-marketing" aria-selected="true">Marketing Services</a>
        <a class="nav-item nav-link" id="pills-events-tab" href="#pills-events" data-toggle="pill" role="tab" aria-controls="pills-events" aria-selected="false">Events Management</a>
        <a class="nav-item nav-link" id="pills-creative-tab" href="#pills-creative" data-toggle="pill" role="tab" aria-controls="pills-creative" aria-selected="false">Creative Services</a>
        <a class="nav-item nav-link" id="pills-talent-tab" href="#pills-talent" data-toggle="pill" role="tab" aria-controls="pills-talent" aria-selected="false">Talent Management</a>
    </nav>
    <!-- Contents of Services by Cards -->
    <div class="tab-content mb-5" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-marketing" role="tabpanel" aria-labelledby="pills-marketing-tab">
            <div class="row">
                <div class="col-sm-12">
                    <h6>Marketing Services</h6>
                    <p>Online Promotions | ATL & BTL Marketing | Guerilla Marketing | Market Research | Secret Shopper</p>
                </div>
            </div>
            <?php if( $marketing->have_posts() ) { ?>
            <div class="row">
            <?php    

                while ( $marketing->have_posts() ) : $marketing->the_post(); 

                // Get categories assigned to a post.
                $taxonomy = 'category';

                // Get the term IDs assigned to post.
                $post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
                    
                // Separator between links.
                $separator = ', ';
                    
                if ( ! empty( $post_terms ) && ! is_wp_error( $post_terms ) ) {
                    
                    $term_ids = implode( ',' , $post_terms );
                    
                    $terms = wp_list_categories( array(
                        'title_li' => '',
                        'style'    => 'list',
                        'echo'     => false,
                        'taxonomy' => $taxonomy,
                        'include'  => $term_ids,
                    ) );
                    
                    $terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
                }
            
            ?>
                <div class="col-sm-4 col-pad-2">
                    <div class="card">
                        <?php if( has_post_thumbnail() ) : ?>
                            <img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                        <?php else : ?>
                            <?php the_content(); ?>
                        <?php endif; ?>
                        <div class="card-body">
                            <h6 class="card-title"><?php the_title(); ?></h6>
                            <p class="card-text"><?php the_excerpt() ?></p>
                        </div>
                    </div>
                </div>
            
            <?php endwhile; ?>

            </div> <!-- end of .row -->
            <?php } ?>
        </div>
        <div class="tab-pane fade" id="pills-events" role="tabpanel" aria-labelledby="pills-events-tab">
            <div class="row">
                <div class="col-sm-12">
                    <h6>Events Management</h6>
                    <p>Corporate Events | LGU Events | Personal Celebrations | Booth Fabrication & Set-up</p>
                </div>
            </div>
            <?php if( $events->have_posts() ) { ?>
            <div class="row">
            <?php    

                while ( $events->have_posts() ) : $events->the_post(); 

                // Get categories assigned to a post.
                $taxonomy = 'category';

                // Get the term IDs assigned to post.
                $post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
                    
                // Separator between links.
                $separator = ', ';
                    
                if ( ! empty( $post_terms ) && ! is_wp_error( $post_terms ) ) {
                    
                    $term_ids = implode( ',' , $post_terms );
                    
                    $terms = wp_list_categories( array(
                        'title_li' => '',
                        'style'    => 'list',
                        'echo'     => false,
                        'taxonomy' => $taxonomy,
                        'include'  => $term_ids,
                    ) );
                    
                    $terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
                }
            
            ?>
                <div class="col-sm-4 col-pad-2">
                    <div class="card">
                        <?php if( has_post_thumbnail() ) : ?>
                            <img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                        <?php else : ?>
                            <?php the_content(); ?>
                        <?php endif; ?>
                        <div class="card-body">
                            <h6 class="card-title"><?php the_title(); ?></h6>
                            <p class="card-text"><?php the_excerpt() ?></p>
                        </div>
                    </div>
                </div>
            
            <?php endwhile; ?>

            </div> <!-- end of .row -->
            <?php } ?>
        </div>
        <div class="tab-pane fade" id="pills-creative" role="tabpanel" aria-labelledby="pills-creative-tab">
            <div class="row">
                <div class="col-sm-12">
                    <h6>Creative Services</h6>
                    <p>Graphic Design | Logo Design | Video Documentation | Packaging | Printed Materials | Company Give-aways | Web Design Services</p>
                </div>
            </div>
            <?php if( $creative->have_posts() ) { ?>
            <div class="row">
            <?php    

                while ( $creative->have_posts() ) : $creative->the_post(); 

                // Get categories assigned to a post.
                $taxonomy = 'category';

                // Get the term IDs assigned to post.
                $post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
                    
                // Separator between links.
                $separator = ', ';
                    
                if ( ! empty( $post_terms ) && ! is_wp_error( $post_terms ) ) {
                    
                    $term_ids = implode( ',' , $post_terms );
                    
                    $terms = wp_list_categories( array(
                        'title_li' => '',
                        'style'    => 'list',
                        'echo'     => false,
                        'taxonomy' => $taxonomy,
                        'include'  => $term_ids,
                    ) );
                    
                    $terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
                }
            
            ?>
                <div class="col-sm-4 col-pad-2">
                    <div class="card">
                        <?php if( has_post_thumbnail() ) : ?>
                            <img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                        <?php else : ?>
                            <?php the_content(); ?>
                        <?php endif; ?>
                        <div class="card-body">
                            <h6 class="card-title"><?php the_title(); ?></h6>
                            <p class="card-text"><?php the_excerpt() ?></p>
                        </div>
                    </div>
                </div>
            
            <?php endwhile; ?>

            </div> <!-- end of .row -->
            <?php } ?>
        </div>
        <div class="tab-pane fade" id="pills-talent" role="tabpanel" aria-labelledby="pills-talent-tab">
            <div class="row">
                <div class="col-sm-12">
                    <h6>Talent Management</h6>
                    <p>Celebrity Bookings | Host and Voice-Over Services | Acoustic Duo and Bands | Ushering Services | Talent Supply for TV and Movie</p>
                </div>
            </div>
            <?php if( $talent->have_posts() ) { ?>
            <div class="row">
            <?php    

                while ( $talent->have_posts() ) : $talent->the_post(); 

                // Get categories assigned to a post.
                $taxonomy = 'category';

                // Get the term IDs assigned to post.
                $post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
                    
                // Separator between links.
                $separator = ', ';
                    
                if ( ! empty( $post_terms ) && ! is_wp_error( $post_terms ) ) {
                    
                    $term_ids = implode( ',' , $post_terms );
                    
                    $terms = wp_list_categories( array(
                        'title_li' => '',
                        'style'    => 'list',
                        'echo'     => false,
                        'taxonomy' => $taxonomy,
                        'include'  => $term_ids,
                    ) );
                    
                    $terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
                }
            
            ?>
                <div class="col-sm-4 col-pad-2">
                    <div class="card">
                        <?php if( has_post_thumbnail() ) : ?>
                            <img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                        <?php else : ?>
                            <?php the_content(); ?>
                        <?php endif; ?>
                        <div class="card-body">
                            <h6 class="card-title"><?php the_title(); ?></h6>
                            <p class="card-text"><?php the_excerpt() ?></p>
                        </div>
                    </div>
                </div>
            
            <?php endwhile; ?>

            </div> <!-- end of .row -->
            <?php } ?>
        </div>
    </div>
    
</div>

<?php get_footer(); ?>