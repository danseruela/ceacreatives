<?php
/**
 * The template for displaying the What We Do Section
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */
?>
<div id="whatwedo" class="card-deck text-center">
    <h1>WHAT WE DO</h1><br>
    <p class="text-center text-muted" style="margin: 0 auto;">
        We are passionate with what we do, and we make sure that we deliver the requirements that our clients need. <br>
        We offer a wide array of well packaged Creative and Marketing services.
    </p>
</div>
<div id="whatwedo-services" class="container">
    <div class="spacer"></div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <a href="<?php home_url() ?>/what-we-do" class="btn btn-danger">CLICK HERE TO LEARN MORE</a>
        </div>
    </div>
    <div class="spacer"></div>
    <div class="spacer"></div>
    <div class="row text-center services">
        <div class="col-sm-3">
            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/marketing-01.png" alt="" class="whatwedo-icon mb-3">
            <h6>Marketing Services</h6>
            <ul class="text-muted">
                <li>Online Promotions</li>
                <li>ATL & BTL Marketing</li>
                <li>Guerilla Marketing</li>
                <li>Market Research</li>
                <li>Secret Shopper</li>
            </ul>
        </div>
        <div class="col-sm-3">
            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/event-01.png" alt="" class="whatwedo-icon mb-3">
            <h6>Events Management</h6>
            <ul class="text-muted">
                <li>Corporate Events</li>
                <li>LGU Events</li>
                <li>Personal Celebrations</li>
                <li>Booth Fabrication & Set-up</li>
            </ul>
        </div>
        <div class="col-sm-3">
            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/creative-01.png" alt="" class="whatwedo-icon mb-3">
            <h6>Creative Services</h6>
            <ul class="text-muted">
                <li>Graphic Design</li>
                <li>Logo Design</li>
                <li>Video Documentation</li>
                <li>Packaging</li>
                <li>Printed Materials</li>
                <li>Company Give-aways</li>
                <li>Web Design Services</li>
            </ul>
        </div>
        <div class="col-sm-3">
            <img src="<?php echo get_template_directory_uri(); ?>/img/icons/talent-01.png" alt="" class="whatwedo-icon mb-3">
            <h6>Talent Management</h6>
            <ul class="text-muted">
                <li>Celebrity Bookings</li>
                <li>Host and Voice-Over Services</li>
                <li>Acoustic Duo and Bands</li>
                <li>Ushering Services</li>
                <li>Talent Supply for TV and Movie.</li>
            </ul>
        </div>
    </div>
</div>