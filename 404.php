<?php
/**
 * The template for displaying 404 pages (not found)
 *
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div class="container text-center" style="margin-top: 7rem;">    
    <div>
        <h1>Error 404</h1>
        <p class="text-muted">Oops! That page can't be found.</p>
        <a class="btn btn-dark" href="<?php echo home_url(); ?>"><i class="fa fa-home"></i> Back To Homepage</a>    
    </div>
    <div class="spacer"></div>
    <div>
        <img src="<?php echo get_template_directory_uri(); ?>/img/error-404.png" alt="ERROR 404 Page not found">
    </div>
	<div class="spacer"></div>
</div><!-- .container -->

<?php get_footer();
