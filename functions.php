<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ceacreative_setup() {
	
  /*
   * Enables custom logo support.
   */
	add_theme_support( 'custom-logo', array(
		'width'       => 500,
		'height'      => 110,
		'flex-width'  => true,
	) );

  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
   */
  add_theme_support( 'post-thumbnails' );
  set_post_thumbnail_size( 1024, 576, true );


  // This theme uses wp_nav_menu() in two locations.
  register_nav_menus( array(
      'header-menu'   => __( 'Header Menu' )
    )
  );

  /*
   * Enable support for Post Formats.
   *
   * See: https://codex.wordpress.org/Post_Formats
   */
  add_theme_support( 'post-formats', array(
    'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
  ) );

  /*
   * HTML5 theme support for comment list.
   */
  add_theme_support( 'html5', array( 'comment-list' ) );

  /*
   * HTML5 theme support for search form.
   */
  add_theme_support( 'html5', array('search-form') );
}

add_action( 'after_setup_theme', 'ceacreative_setup' );

function ceacreative_the_custom_logo() {
	
	if ( function_exists( 'the_custom_logo' ) ) {
		the_custom_logo();
	}

}

/*
 * Custom User Login Logo
 */
function ceacreative_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
          background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/cealogo-black-v2.png);
          height:36px;
          width:320px;
          background-size: 320px auto;
          background-repeat: no-repeat;
          padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'ceacreative_login_logo' );

function ceacreative_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'ceacreative_login_logo_url' );

function ceacreative_login_logo_url_title() {
    return 'Powered by dseruela web designs';
}
add_filter( 'login_headertitle', 'ceacreative_login_logo_url_title' );

/*
 * Add styles and scripts
 */
function ceacreative_enqueue_styles() {
	  wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style('fontawesome', get_stylesheet_directory_uri() . '/css/all.min.css' );
	  wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css' );

    wp_enqueue_script( 'bootstrap-script', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), '1.0.0', true );
    
}
add_action( 'wp_enqueue_scripts', 'ceacreative_enqueue_styles' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ceacreative_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Homepage - Service Section', 'ceacreative' ),
    'id'            => 'sidebar-6',
    'description'   => __( 'Add widgets here to appear in your services section.', 'ceacreative' ),
    'before_widget' => '<div class="col-sm-4 my-5">',
    'after_widget'  => '</div>',

  ) );

  register_sidebar( array(
    'name'          => __( 'Sidebar', 'ceacreative' ),
    'id'            => 'sidebar-1',
    'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'ceacreative' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Footer 1', 'ceacreative' ),
    'id'            => 'sidebar-2',
    'description'   => __( 'Add widgets here to appear in your footer.', 'ceacreative' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Footer 2', 'ceacreative' ),
    'id'            => 'sidebar-3',
    'description'   => __( 'Add widgets here to appear in your footer.', 'ceacreative' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => __( 'Footer 3', 'ceacreative' ),
    'id'            => 'sidebar-4',
    'description'   => __( 'Add widgets here to appear in your footer.', 'ceacreative' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  // register_sidebar( array(
  //   'name'          => __( 'Footer 4', 'ceacreative' ),
  //   'id'            => 'sidebar-5',
  //   'description'   => __( 'Add widgets here to appear in your footer.', 'ceacreative' ),
  //   'before_widget' => '<section id="%1$s" class="widget %2$s">',
  //   'after_widget'  => '</section>',
  //   'before_title'  => '<h3 class="widget-title">',
  //   'after_title'   => '</h3>',
  // ) );

}
add_action( 'widgets_init', 'ceacreative_widgets_init' );

// Adding Custom Menu Links
add_filter( 'nav_menu_link_attributes', 'add_menu_atts', 10, 3 );

function add_menu_atts( $atts, $item, $args ) {

  switch ( get_the_title( $item->object_id ) ) {

    case 'What We Do': $atts['href'] = is_front_page() ? '#whatwedo' : home_url().'/#whatwedo'; break;
    
    case 'About':  $atts['href'] = is_front_page() ? '#about' : home_url().'/#about'; break;

    case 'Portfolio': $atts['href'] = is_front_page() ? '#portfolio' : home_url().'/#portfolio'; break;

    case 'Contact Us': $atts['href'] = is_front_page() ? '#contact' : home_url().'/#contact'; break;

    default: $atts['href'] = $item->url; break;
  }

  return $atts;
}

?>