<?php
/**
 * Template Name: Rentals Page Template
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */

get_header(); 

    $rentals = new WP_Query( array(
        'category_name' => 'rentals',
        'posts_per_page' => 100,
    ) );

?>
<div id="rentals-page" class="card-deck">
    <h1 class="text-center">Event Needs Rentals</h1><br>
    <p class="text-center text-muted" style="margin: 0 auto;">
        We are passionate with what we do, and we make sure that we deliver the requirements that our clients need. <br>
        We offer a wide array of well packaged Creative and Marketing services.
    </p>

    <!-- Contents of Services by Cards -->
    <?php if( $rentals->have_posts() ) { ?>
    <div class="row mt-5 mb-5">
    <?php    

        while ( $rentals->have_posts() ) : $rentals->the_post(); 

        // Get categories assigned to a post.
        $taxonomy = 'category';

        // Get the term IDs assigned to post.
        $post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
            
        // Separator between links.
        $separator = ', ';
            
        if ( ! empty( $post_terms ) && ! is_wp_error( $post_terms ) ) {
            
            $term_ids = implode( ',' , $post_terms );
            
            $terms = wp_list_categories( array(
                'title_li' => '',
                'style'    => 'list',
                'echo'     => false,
                'taxonomy' => $taxonomy,
                'include'  => $term_ids,
            ) );
            
            $terms = rtrim( trim( str_replace( '<br />',  $separator, $terms ) ), $separator );
        }
    
    ?>
        <div class="col-sm-4 col-pad-2">
            <div class="card">
                <img class="card-img-top" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>">
                <div class="card-body">
                    <h6 class="card-title"><?php the_title(); ?></h6>
                    <p class="card-text"><?php the_excerpt() ?></p>
                    <div class="text-center">
                        <a id="getquote" class="btn btn-dark" href="#getquoteform">Get Quote</a>
                    </div>
                </div>
            </div>
        </div>
    
    <?php endwhile; ?>

    </div> <!-- end of .row -->
    <?php } ?>

    <div id="getquoteform">
        <h1 class="text-center">GET QUOTE</h1>
        <div class="contactLeft">
            <?php
            // Display Get Quote Form using shortcode.
            echo do_shortcode( '[contact-form-7 id="296" title="Get Quote"]' );
            ?>    
        </div>
        <div class="contactRight">
            <h6>Visayas Office</h6>
            <p><i class="fas fa-user-alt"></i> Jason Jasten Silvano</p>
            <p><i class="fas fa-map-marker-alt"></i> #09 SAS Building, Pres. Magsaysay St., Mabolo, Cebu City</p>
            <p><i class="fas fa-mobile-alt"></i> 0977 293 9645</p>
            <p><i class="fas fa-envelope"></i> jasten@ceacreatives.com</p>
            <div class="spacer"></div>
            <h6>Mindanao Satellite Office</h6>
            <p><i class="fas fa-user-alt"></i> Angela Therese Uytengsu - Villamera</p>
            <p><i class="fas fa-map-marker-alt"></i> Door 3, Obrero Suites, Palma Gil St, Obrero Davao City</p>
            <p><i class="fas fa-mobile-alt"></i> 09173339515</p>
            <p><i class="fas fa-envelope"></i> kimo@ceacreatives.com</p>
        </div>
    </div>
</div>
    
</div>

<?php get_footer(); ?>