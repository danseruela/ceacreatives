<?php
/**
 * Displays header main menu if assigned
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */

$custom_logo_id = get_theme_mod( 'custom_logo' );
$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
?>

<nav id="navbar-main" class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">
      <?php if(!empty($image[0])) { ?>
          <img class="img-fluid cclogo" alt="CEA Creatives Logo" src="<?php echo $image[0]; ?>">
      <?php 
        } else {
            echo "CEA Creatives";
      }?>
      <div class="site-description"><?php bloginfo('description'); ?></div>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
      <?php 
        wp_nav_menu( array( 
            'theme_location'  => 'header-menu', 
            'container_class' => 'collapse navbar-collapse justify-content-end', 
            'container_id'    => 'navbarSupportedContent',
            'menu_class'      => 'navbar-nav text-uppercase',
            'menu_id'         => '',
            'echo'            =>  true
            ) ); 
      ?>
    </div>
  </nav>

