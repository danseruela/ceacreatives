<?php
/**
 * Displays footer site info
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */

?>
<div class="copyright text-center">	
	Copyright © 2021 <a href="/">CEA Creatives and Events</a><br>	
	<!-- Maintained by <a href="<?php //echo esc_url( __( '#', 'CEACreative' ) ); ?>"><?php //printf( __( ' %s', 'CEACreative' ), 'djtechsolutions.com' ); ?></a> -->
</div><!-- .site-info -->
