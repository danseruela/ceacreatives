<?php
/**
 * The template for displaying the Contact Section
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */
?>
<div id="contact" class="card-deck">
    <h1 class="text-center">GET IN TOUCH</h1>
    <div class="contactLeft">
        <?php
        // Display Contact Us Form using shortcode.
        echo do_shortcode( '[contact-form-7 id="23" title="Contact Us Form"]' );
        ?>
    </div>
    <div class="contactRight">
        <h6>CEA Creatives & Events</h6>
        <!-- <p><i class="fas fa-user-alt"></i> Mary Grace Albano </p> -->
        <p><i class="fas fa-map-marker-alt"></i> Door 6, TAA Center, F Cabahug St, Brgy. Kasambagan, Mabolo, Cebu City, Philippines, 6000</p>
        <p><i class="fas fa-envelope"></i> info@ceacreatives.com</p>
        <p><i class="fas fa-mobile-alt"></i> 0917 333 9515</p>
        <div class="spacer"></div>
    </div>    
</div>