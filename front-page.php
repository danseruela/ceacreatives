<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */

get_header(); 
?>
<div class="spacer"></div>
<?php
// Display What We Do Section.
get_template_part( 'template-parts/section/section', 'whatwedo' );
?>
<div class="spacer"></div>
<?php
// Display Company Overview Section.
get_template_part( 'template-parts/section/section', 'about' );
?>
<div class="spacer"></div>
<?php
// Display Portfolio Section.
get_template_part( 'template-parts/section/section', 'portfolio' );
?>
<div class="spacer"></div>
<?php
// Display Contact Us Section.
get_template_part( 'template-parts/section/section', 'contact' );

get_footer(); 
?>   