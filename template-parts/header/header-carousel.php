<?php
/**
 * Displays header carousel if assigned
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */

?>
<header>
    <div id="carouselHeaderControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="<?php echo get_template_directory_uri(); ?>/img/sliders/cea.creatives-slider-1.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/sliders/cea.creatives-slider-marketing.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/sliders/cea.creatives-slider-events.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/sliders/cea.creatives-slider-creative.jpg" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img src="<?php echo get_template_directory_uri(); ?>/img/sliders/cea.creatives-slider-talent.jpg" class="d-block w-100" alt="...">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselHeaderControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselHeaderControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    </div>
</header>