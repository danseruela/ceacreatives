<?php
/**
 * The template for displaying the About Section
 *
 * @package CEA Creatives
 * @subpackage CEACreative
 * @since 1.0
 * @version 1.0
 */
?>
<div id="about">
    <div class="aboutLeft">
        <h1 class="aboutH1">COMPANY<br>OVERVIEW</h1>
        <p>CEA Creatives & Events is a full-service corporate and government marketing and events start-up agency founded in 2016 by Kimo Cea with its main office based in Cebu City, with satellite offices in Paranaque City and Cagayan de Oro.</p>
        <p>Our team is always in tune to the dynamic trends, and we always make sure that our hard work and dedication translate to great results.</p>
        <p>We are your well-trusted award-winning agency partner in engaging and impacting your target market and creating your creative concepts come to life.</p>
        <p>In CEA, you are our boss.</p>
    </div>
</div>